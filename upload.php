<?php
    $target_file = $_FILES["fileToUpload"]["tmp_name"]; // Get the temporary location of the file on the server
    $xmlDoc = new DOMDocument();
    $xmlDoc->load($target_file); // load the xml file
    $mysql_hostname = "localhost"; // Name of host
    $mysql_user = "root"; // User name
    $mysql_password = "1234"; // Password
    $mysql_database = "new_xml_extract"; // Name of the database

    // Connect to the database
    $dbh = new PDO("mysql:dbname={$mysql_database};host={$mysql_hostname};port=3306", $mysql_user, $mysql_password);

    $dbh->query("TRUNCATE TABLE text_xml"); // Empty the database

    $xmlObject = $xmlDoc->getElementsByTagName('comment'); // Name of the most outer element in the system
    $commentCount = $xmlObject->length;

    for ($i=0; $i < $commentCount; $i++){
        ini_set('max_execution_time', 3000); // Set execution time of the loop

        // Get every element needed from the xml file
        $CommentId = $xmlObject->item($i)->getElementsByTagName('CommentId')->item(0)->childNodes->item(0)->nodeValue;
        $UserId = $xmlObject->item($i)->getElementsByTagName('UserId')->item(0)->childNodes->item(0)->nodeValue;
        $VideoName = $xmlObject->item($i)->getElementsByTagName('VideoName')->item(0)->childNodes->item(0)->nodeValue;
        $Aspect = $xmlObject->item($i)->getElementsByTagName('Aspect')->item(0)->childNodes->item(0)->nodeValue;
        $Cuepoint = $xmlObject->item($i)->getElementsByTagName('Cuepoint')->item(0)->childNodes->item(0)->nodeValue;
        $CommentDate = $xmlObject->item($i)->getElementsByTagName('CommentDate')->item(0)->childNodes->item(0)->nodeValue;
        $Comment = $xmlObject->item($i)->getElementsByTagName('Comment')->item(0)->childNodes->item(0)->nodeValue;

        // Insert the values into the database
        $sql = $dbh->prepare("INSERT INTO `text_xml` (`CommentId`, `UserId`, `VideoName`, `Aspect`, `Cuepoint`, `CommentDate`, `Comment`) VALUES (?, ?, ?, ?, ?, ?, ?)");

        $sql->execute(array(
        $CommentId,
        $UserId,
        $VideoName,
        $Aspect,
        $Cuepoint,
        $CommentDate,
        $Comment
        ));

        // Redirect to visualisation page
        header( 'Location: http://localhost/chartjs/bargraph.html' );
    }
?>