# README #

In the section below I will explain how to run the software for the project.

### How to get the software running###

1. Download and install XAMPP
2. Go to the xampp\htdocs directory and copy the git repository in there 
3. Open XAMPP Control Panel and start Apache and MySQL
4. Open a web browser and go to http://localhost/phpmyadmin
5. Set up a password: 1234
6. Create a new database and name it 'new_xml_extract'
7. Inside it create a table called text_xml with the following columns: CommentId(int), UserId(int), VideoName(text), Aspect(text), Cuepoint(int)	CommentDate(timestamp), Comment(text)
8. Go to http://localhost/ and everything should be working correctly