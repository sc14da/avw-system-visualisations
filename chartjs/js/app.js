$(document).ready(function(){
	$.ajax({
		// Get the data for the chart
		url: "http://localhost/chartjs/bardata.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var videon = [];
			var score = [];
			var userid = [];
			var numcomments = [];
			var users = [];

			// Get the different values into an array 
			for(var i in data) {
				videon.push(data[i].VideoName);
				score.push(data[i].CommentId);
				userid.push(data[i].NumberOfUsers);
			}

			// Load the data into the chart
			var chartdata = {
				labels: videon,
				datasets : [
				{
					label: 'Comments',
					backgroundColor: "#1F8261",
					borderColor: 'white',
					hoverBackgroundColor: '#A5D6A7',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: score,
					borderWidth: 2
				},
				{
					label: 'Users',
					backgroundColor: "#FFA500",
					borderColor: 'white',
					hoverBackgroundColor: '#FFD54F',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: userid,
					borderWidth: 2
				},
				]
			};

			var ctx = $("#mycanvas");
			ctx.attr('height',250);

			// Create the chart and its options
			var barGraph = new Chart(ctx, {
				type: 'bar',
				data: chartdata,
				options: {

					barValueSpacing: 20,
					tooltips: {
						mode: 'label'
					},
					responsive: true,
					segmentShowStroke: true,
					scales: {
						xAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Videos'
							},
							display: true,
							ticks: {
								userCallback: function(label, index, labels) {
									if(typeof label === "string")
									{
										return label.substring(0,15)
									}
									return label

								},
							},
							gridLines: {
								lineWidth: 0,
								color: "#9E9E9E"
							}
						}],
						yAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Count'
							},
							ticks: {
								min: 0,
								stepSize: 10,
							},
							gridLines: {
								lineWidth: 0,
								color: "#9E9E9E"
							}
						}]
					}
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});

	$.ajax({
		// Get the data for the chart
		url: "http://localhost/chartjs/radardata.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var numcomments = [];
			var users = [];

			// Get the different values into an array 
			for(var i in data) {
				numcomments.push(data[i].AspectNumber);
				users.push(data[i].Aspect);
			}

			// Load the data into the chart
			var chartdata1 = {
				labels: users,
				datasets: [
				{
					label: "Selected",
					lineTension: 0.1,
					backgroundColor: "rgba(178, 223, 219,0.2)",
					borderColor: "#009688",
					pointBackgroundColor: "#004D40",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "rgba(255,99,132,1)",
					data: numcomments
				}
				]
			};

			Chart.defaults.global.defaultFontColor = '#E0E0E0';
			Chart.defaults.scale.gridLines.color = "#9E9E9E";
			Chart.defaults.scale.gridLines.zeroLineColor = "#9E9E9E";

			var ctx = $("#linechart");

			// Create the chart and its options
			var options = {
				responsive: true,
				maintainAspectRadio: true,
				tooltips: {
					enable: true,
					mode: 'label',
					callbacks: {
						label: function(tooltipItem, data){
							var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
							return datasetLabel + ': ' + Number(tooltipItem.yLabel) + ' Times';
						}
					}
				},
				legend: {
					position: 'top',
				},
				title: {
					display: false,
				},
				scale: {
					gridLines: {
						color: "#9E9E9E",
					},
					angleLines: {
						color: '#9E9E9E'
					},
					reverse: false,
					ticks: {
						showLabelBackdrop: false,
						min: 0,
						beginAtZero: false,
						stepSize: 30
					},
				},
				scaleOverride: false,
				scaleSteps: 5,
				scaleStepWidth: 20,
				scaleStartValue: 100,
			};

			var LineGraph = new Chart(ctx, {
				type: 'radar',
				data: chartdata1, 
				options: options
			});
		},
		error: function(data) {
			console.log(data);
		}
	});

	$.ajax({
		// Get the data for the chart
		url: "http://localhost/chartjs/doughnutdata.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var over50 = [];
			var over40 = [];
			var over30 = [];
			var over20 = [];
			var over10 = [];

			// Get the different values into an array 
			for(var i in data) {
				over50.push(data[i].Over50);
				over40.push(data[i].Over40);
				over30.push(data[i].Over30);
				over20.push(data[i].Over20);
				over10.push(data[i].Over10);
			}

			// Load the data into the chart


			var data = {
				labels: [
				"Over 50 Comments",
				"30 to 40 Comments",
				"20 ot 30 Comments",
				"10 to 20 Comments",
				"0 to 10 Comments"
				],
				datasets: [
				{
					data: [over50, over40, over30, over20, over10],
					backgroundColor: [
					"#FF6384",
					"#36A2EB",
					"#a5d6a7",
					"#00897b",
					"#FFCE56"
					],
					hoverBackgroundColor: [
					"#FF6384",
					"#36A2EB",
					"#a5d6a7",
					"#00897b",
					"#FFCE56"
					]	
				}]
			};



			var ctx = $("#donutchart");

			var LineGraph = new Chart(ctx,{
				type:"doughnut",
				data: data,
				options: {
					  pieceLabel: {
					    mode: 'percentage',
					    precision: 2
					  },
					animation:{
						animateScale:true
					}
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});

	$.ajax({
		// Get the data for the chart
		url: "http://localhost/chartjs/piedata.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var users = [];
			var aspects= [];
			var videos = [];

			// Get the different values into an array 
			for(var i in data) {
				users.push(data[i].Users);
				aspects.push(data[i].Aspects);
				videos.push(data[i].Videos);
			}

			// Load the data into the chart
			var data = {
				labels: [
				"Users",
				"Aspects",
				"Videos"
				],
				datasets: [
				{
					data: [users, aspects, videos],
					backgroundColor: [
					"#FF6384",
					"#36A2EB",
					"#a5d6a7",
					"#00897b",
					"#FFCE56"
					],
					hoverBackgroundColor: [
					"#FF6384",
					"#36A2EB",
					"#a5d6a7",
					"#00897b",
					"#FFCE56"
					]
				}]
			};

			var ctx = $("#piechart");

			var LineGraph = new Chart(ctx,{
				type:"pie",
				data: data,
				options: {
					animation:{
						animateScale:true
					}
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});

	$.ajax({
		// Get the data for the chart
		url: "http://localhost/chartjs/stackedbardata.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var videon = [];
			var min1 = [];
			var min2 = [];
			var min3 = [];
			var min4 = [];
			var min5 = [];
			var min6 = [];
			var min7 = [];
			var min8 = [];
			var min9 = [];
			var min10 = [];
			var min11 = [];

			// Get the different values into an array 
			for(var i in data) {
				videon.push(data[i].VideoName);
				min1.push(data[i].min1);
				min2.push(data[i].min2);
				min3.push(data[i].min3);
				min4.push(data[i].min4);
				min5.push(data[i].min5);
				min6.push(data[i].min6);
				min7.push(data[i].min7);
				min8.push(data[i].min8);
				min9.push(data[i].min9);
				min10.push(data[i].min10);
				min11.push(data[i].min11);
			}

			// Load the data into the chart
			var chartdata = {
				labels: videon,
				datasets : [
				{
					label: 'First Minute',
					backgroundColor: 'rgba(39, 174, 96, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(39, 174, 96, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min1,
					borderWidth: 2
				},
				{
					label: 'Secound Minute',
					backgroundColor: 'rgba(230,126,34 ,1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(230,126,34 ,1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min2,
					borderWidth: 2
				},
				{
					label: 'Third Minute',
					backgroundColor: 'rgba(26, 188, 156, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(26, 188, 156, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min3,
					borderWidth: 2
				},
				{
					label: 'Fourth Minute',
					backgroundColor: 'rgba(52, 152, 219, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(52, 152, 219, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min4,
					borderWidth: 2
				},
				{
					label: 'Fifth Minute',
					backgroundColor: 'rgba(241,196,15 ,1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(241,196,15 ,1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min5,
					borderWidth: 2
				},
				{
					label: 'Sixth Minute',
					backgroundColor: 'rgba(31, 97, 141, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(31, 97, 141, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min6,
					borderWidth: 2
				},
				{
					label: 'Seventh Minute',
					backgroundColor: 'rgba(231,76,60 ,1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(231,76,60 ,1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min7,
					borderWidth: 2
				},
				{
					label: 'Eight Minute',
					backgroundColor: 'rgba(44, 62, 80, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(44, 62, 80, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min8,
					borderWidth: 2
				},
				{
					label: 'Nineth Minute',
					backgroundColor: 'rgba(118, 68, 138, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(118, 68, 138, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min9,
					borderWidth: 2
				},
				{
					label: 'Tenth Minute',
					backgroundColor: 'rgba(81, 46, 95, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(81, 46, 95, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min10,
					borderWidth: 2
				},
				{
					label: 'Eleventh Minute',
					backgroundColor: 'rgba(21, 67, 96, 1)',
					borderColor: 'rgba(255, 235, 238, 1)',
					hoverBackgroundColor: 'rgba(21, 67, 96, 1)',
					hoverBorderColor: 'rgba(200, 200, 200, 1)',
					data: min11,
					borderWidth: 2
				},
				]
			};

			var ctx = $("#newchart");
			ctx.attr('height',250);

			var barGraph = new Chart(ctx, {
				type: 'horizontalBar',
				data: chartdata,
				options: {
					pieceLabel: {
					    mode: 'percentage',
					    precision: 2
					  },
					barValueSpacing: 20,
					tooltips: {
						enable: true,
						mode: 'label',
						callbacks: {
							label: function(tooltipItem, data){
								var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
								return datasetLabel + ': ' + Number(tooltipItem.xLabel) + ' Users';
							}
						}
					},
					responsive: true,
					segmentShowStroke: true,
					scales: {
						xAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Users'
							},
							stacked: true,
							display: true,
							gridLines: {
								lineWidth: 0,
								color: "#9E9E9E"
							}
						}],
						yAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Videos'
							},
							stacked: true,
							ticks: {
								userCallback: function(label, index, labels) {
									if(typeof label === "string")
									{
										return label.substring(0,25)
									}
									return label

								},
							},
							gridLines: {
								lineWidth: 0,
								color: "#9E9E9E"
							}
						}]
					}
				}
			});
		},
		error: function(data) {
			console.log(data);
		}
	});

var url = 'http://localhost/chartjs/linedata.php'
$.getJSON(url,
    function (json) {
        var tr;
        for (var i = 0; i < json.length; i++) {
            tr = $('<tr/>');
            tr.append("<td>" + json[i].Users + "</td>");
            tr.append("<td>" + json[i].Comments + "</td>");
            tr.append("<td>" + json[i].Aspects + "</td>");
            tr.append("<td>" + json[i].Videos + "</td>");
            $('table').append(tr);
        }
    });

});


